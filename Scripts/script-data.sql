USE [C:\USERS\CAROL\SOURCE\REPOS\GESTORUSUARIOS\WCFSERVICIOUSUARIOS\APP_DATA\DB_PRUEBA_TECNICA.MDF]
GO
INSERT [dbo].[APP_SEXO] ([Codigo], [Nombre]) VALUES (N'F', N'Femenino')
INSERT [dbo].[APP_SEXO] ([Codigo], [Nombre]) VALUES (N'M', N'Masculino')
GO
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Alexis Cordoba', CAST(N'1989-06-19' AS Date), N'M')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Camilo Lozano', CAST(N'1992-07-20' AS Date), N'M')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Carol Tascon', CAST(N'1993-07-18' AS Date), N'F')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Diego Valencia', CAST(N'1964-11-29' AS Date), N'M')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Emiliano Balvin', CAST(N'2013-04-25' AS Date), N'M')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Jorge Lozano Izquierdo', CAST(N'1964-11-16' AS Date), N'M')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Juan Arcila', CAST(N'1990-06-26' AS Date), N'F')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Jurgen Lozano Valencia', CAST(N'1991-01-30' AS Date), N'M')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Leandro Acevedo', CAST(N'1989-12-13' AS Date), N'M')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Silvana Lozano', CAST(N'2021-05-27' AS Date), N'F')
INSERT [dbo].[APP_USUARIOS] ([Nombre], [Fecha_Nacimiento], [Sexo]) VALUES (N'Silvia Valencia', CAST(N'1968-07-22' AS Date), N'F')
GO
