USE [C:\USERS\CAROL\SOURCE\REPOS\GESTORUSUARIOS\WCFSERVICIOUSUARIOS\APP_DATA\DB_PRUEBA_TECNICA.MDF]
GO
/****** Object:  Table [dbo].[APP_SEXO]    Script Date: 16/06/2023 2:56:03 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[APP_SEXO](
	[Codigo] [char](1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[APP_USUARIOS]    Script Date: 16/06/2023 2:56:03 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[APP_USUARIOS](
	[Nombre] [varchar](100) NOT NULL,
	[Fecha_Nacimiento] [date] NOT NULL,
	[Sexo] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APP_USUARIOS]  WITH CHECK ADD FOREIGN KEY([Sexo])
REFERENCES [dbo].[APP_SEXO] ([Codigo])
GO
/****** Object:  Table [dbo].[LOG_AUDITORIA]    Script Date: 16/06/2023 2:56:03 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LOG_AUDITORIA](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Operacion] [varchar](100) NOT NULL,
	[Objeto] [varchar](max) NOT NULL,
	[Fecha] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  StoredProcedure [dbo].[PRO_APP_USUARIOS_ACTUALIZACION]    Script Date: 16/06/2023 2:56:03 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRO_APP_USUARIOS_ACTUALIZACION]
	@Nombre				VARCHAR(100),
	@Fecha_Nacimiento   DATE,
	@Sexo			    CHAR(1),
	@Respuesta			INT OUTPUT
AS
/*******************************************************************************************************
*
* Autor:		Jurgen Lozano <jurgn022@gmail.com>
* Fecha:		15/06/2023
* Descripción:	Controla la actualización de registros en la entidad - APP_USUARIOS
*
* Historia de Modificaciones
* Autor			Fecha			 Descripción
*
********************************************************************************************************/
SET NOCOUNT ON;

BEGIN

	IF EXISTS (SELECT 1 FROM APP_USUARIOS WHERE UPPER(Nombre) = UPPER(RTRIM(LTRIM(@Nombre)))) 

		BEGIN
			UPDATE APP_USUARIOS
			SET Nombre = RTRIM(LTRIM(@Nombre))
			 , Fecha_Nacimiento = @Fecha_Nacimiento 
			 , Sexo = UPPER(@Sexo)			   
			WHERE UPPER(Nombre) = UPPER(RTRIM(LTRIM(@Nombre)));

			SET @Respuesta = 1;
		END

	ELSE
		BEGIN
			SET @Respuesta = 0;
		END	 

END
GO
/****** Object:  StoredProcedure [dbo].[PRO_APP_USUARIOS_CONSULTAR_PAGINADO]    Script Date: 16/06/2023 2:56:03 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRO_APP_USUARIOS_CONSULTAR_PAGINADO]
	@Numero_Pagina 			INT, 
	@Cantidad_Por_Pagina 	INT,
	@Cantidad_Total 		INT OUTPUT,
	@Numero_Pagina_Maximo 	INT OUTPUT
AS
/*******************************************************************************************************
*
* Autor:		Jurgen Lozano <jurgn022@gmail.com>
* Fecha:		15/06/2023
* Descripción:	Controla la consulta de todos los registros de forma paginada de la entidad - APP_USUARIOS
*
* Historia de Modificaciones
* Autor			Fecha			 Descripción
*
********************************************************************************************************/
SET NOCOUNT ON;

	-- Declaración de cursores
	DECLARE CU_CANTIDAD CURSOR LOCAL FOR
  		SELECT COUNT(1)
		  FROM APP_USUARIOS;

	-- Declaración de Variables
	DECLARE @nu_Cantidad INT = 0;

BEGIN
		
	OPEN CU_CANTIDAD;
	FETCH CU_CANTIDAD INTO @nu_Cantidad;
	CLOSE CU_CANTIDAD;
	DEALLOCATE CU_CANTIDAD;

	SET @Cantidad_Total = @nu_Cantidad;
	SET @Numero_Pagina_Maximo = CEILING(@nu_Cantidad/CONVERT(decimal,@Cantidad_Por_Pagina));
	
	SELECT Nombre
		 , Fecha_Nacimiento
		 , Sexo
	  FROM APP_USUARIOS
	  ORDER BY Nombre
	OFFSET (@Numero_Pagina - 1) * @Cantidad_Por_Pagina ROWS FETCH NEXT @Cantidad_Por_Pagina ROWS ONLY;					

END
GO
/****** Object:  StoredProcedure [dbo].[PRO_APP_USUARIOS_CONSULTAR_TODOS]    Script Date: 16/06/2023 2:56:03 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRO_APP_USUARIOS_CONSULTAR_TODOS]
AS
/*******************************************************************************************************
*
* Autor:		Jurgen Lozano <jurgn022@gmail.com>
* Fecha:		15/06/2023
* Descripción:	Controla la consulta de todos los registros de la entidad - APP_USUARIOS
*
* Historia de Modificaciones
* Autor			Fecha			 Descripción
*
********************************************************************************************************/
SET NOCOUNT ON;

BEGIN
		
	SELECT Nombre
		 , Fecha_Nacimiento
		 , Sexo
  	  FROM APP_USUARIOS
	  ORDER BY Nombre;					

END
GO
/****** Object:  StoredProcedure [dbo].[PRO_APP_USUARIOS_ELIMINACION]    Script Date: 16/06/2023 2:56:03 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRO_APP_USUARIOS_ELIMINACION]
	@Nombre				VARCHAR(100),
	@Respuesta			INT OUTPUT
AS
/*******************************************************************************************************
*
* Autor:		Jurgen Lozano <jurgn022@gmail.com>
* Fecha:		15/06/2023
* Descripción:	Controla la eliminación de registros en la entidad - APP_USUARIOS
*
* Historia de Modificaciones
* Autor			Fecha			 Descripción
*
********************************************************************************************************/
SET NOCOUNT ON;

BEGIN
	
	IF EXISTS (SELECT 1 FROM APP_USUARIOS WHERE UPPER(Nombre) = UPPER(RTRIM(LTRIM(@Nombre)))) 
		BEGIN
			DELETE FROM APP_USUARIOS		   
			WHERE UPPER(Nombre) = UPPER(LTRIM(RTRIM(@Nombre)));					
			SET @Respuesta = 1;
		END	
	ELSE
		BEGIN
			SET @Respuesta = 0;
		END

END
GO
/****** Object:  StoredProcedure [dbo].[PRO_APP_USUARIOS_INSERCION]    Script Date: 16/06/2023 2:56:03 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRO_APP_USUARIOS_INSERCION]
	@Nombre				VARCHAR(100),
	@Fecha_Nacimiento   DATE,
	@Sexo			    CHAR(1),
	@Respuesta			INT OUTPUT
AS
/*******************************************************************************************************
*
* Autor:		Jurgen Lozano <jurgn022@gmail.com>
* Fecha:		15/06/2023
* Descripción:	Controla la inserción de nuevos registros en la entidad - APP_USUARIOS
*
* Historia de Modificaciones
* Autor			Fecha			 Descripción
*
********************************************************************************************************/
SET NOCOUNT ON;

BEGIN
	
	IF NOT EXISTS (SELECT 1 FROM APP_USUARIOS WHERE UPPER(Nombre) = UPPER(RTRIM(LTRIM(@Nombre)))) 

		BEGIN 
			INSERT INTO APP_USUARIOS
			( Nombre
			, Fecha_Nacimiento
			, Sexo
			)
			VALUES
			( LTRIM(RTRIM(@Nombre))
			, @Fecha_Nacimiento
			, UPPER(LTRIM(RTRIM(@Sexo)))			
			);							

			SET @Respuesta = 1;
		END
	ELSE
		BEGIN
			SET @Respuesta = 0;
		END

END
GO
