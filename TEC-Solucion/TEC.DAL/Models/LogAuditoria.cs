﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace TEC.DAL.Models
{
    public partial class LogAuditoria
    {
        public int Id { get; set; }
        public string Operacion { get; set; }
        public string Objeto { get; set; }
        public DateTime Fecha { get; set; }
    }
}