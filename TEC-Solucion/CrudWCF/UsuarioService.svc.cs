﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using System.Threading.Tasks;
using TEC.DAL.Models;

namespace CrudWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class UsuarioService : IUsuarioService
    {

        private readonly DB_PRUEBA_TECNICAContextProcedures _procedures;
        private readonly DB_PRUEBA_TECNICAContext context;

        public UsuarioService()
        {
            context = new DB_PRUEBA_TECNICAContext();
            _procedures = new DB_PRUEBA_TECNICAContextProcedures(context);
        }

        public async Task<string> CrearUsuario(PRO_APP_USUARIOS_CONSULTAR_TODOSResult usuario)
        {
            try
            {
                var result = await _procedures.PRO_APP_USUARIOS_INSERCIONAsync(usuario.Nombre, usuario.Fecha_Nacimiento, usuario.Sexo);

                var resultLog = await context.LogAuditoria.AddAsync(new LogAuditoria
                {
                    Fecha = DateTime.Now,
                    Operacion = "CREAR",
                    Objeto = JsonConvert.SerializeObject(usuario)
                });

                await context.SaveChangesAsync();

                if (result == 1)
                {
                    return $"{result}|Usuario creado exitosamente";
                }
                else
                {
                    return $"{result}|Error al crear el usuario";
                }
            }
            catch (Exception)
            {
                return $"Error al crear el usuario - Consulte con el administrador";
            }
        }

        public async Task<string> EliminarUsuario(PRO_APP_USUARIOS_CONSULTAR_TODOSResult usuario)
        {

            try
            {
                var result = await _procedures.PRO_APP_USUARIOS_ELIMINACIONAsync(usuario.Nombre);
                
                var resultLog = await context.LogAuditoria.AddAsync(new LogAuditoria
                {
                    Fecha = DateTime.Now,
                    Operacion = "ELIMINAR",
                    Objeto = JsonConvert.SerializeObject(usuario)
                });

                await context.SaveChangesAsync();

                if (result == 1)
                {
                    return $"{result}|Usuario eliminado exitosamente";
                }
                else
                {
                    return $"{result}|Error al eliminar el usuario";
                }
            }
            catch (Exception)
            {
                return $"Error al eliminar el usuario - Consulte con el administrador";
            }
        }

        public async Task<IEnumerable<PRO_APP_USUARIOS_CONSULTAR_TODOSResult>> GetAllUsuarios()
        {
            return await _procedures.PRO_APP_USUARIOS_CONSULTAR_TODOSAsync();
        }

        public async Task<IEnumerable<PRO_APP_USUARIOS_CONSULTAR_PAGINADOResult>> GetUsuariosPaginado(int numeroPagina, int cantidadPorPagina = 10)
        {
            return await _procedures.PRO_APP_USUARIOS_CONSULTAR_PAGINADOAsync(numeroPagina, cantidadPorPagina);
        }

        public async Task<string> ModificarUsuario(PRO_APP_USUARIOS_CONSULTAR_TODOSResult usuario)
        {
            try
            {
                var result = await _procedures.PRO_APP_USUARIOS_ACTUALIZACIONAsync(usuario.Nombre, usuario.Fecha_Nacimiento, usuario.Sexo);

                var resultLog = await context.LogAuditoria.AddAsync(new LogAuditoria
                {
                    Fecha = DateTime.Now,
                    Operacion = "MODIFICAR",
                    Objeto = JsonConvert.SerializeObject(usuario)
                });

                await context.SaveChangesAsync();

                if (result == 1)
                {
                    return $"{result}|Usuario modificar exitosamente";
                }
                else
                {
                    return $"{result}|Error al modificar el usuario";
                }
            }
            catch (Exception)
            {
                return $"Error al modificar el usuario - Consulte con el administrador";
            }
        }
    }
}
