﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using TEC.DAL.Models;

namespace CrudWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IUsuarioService
    {

        [OperationContract]
        Task<IEnumerable<PRO_APP_USUARIOS_CONSULTAR_TODOSResult>> GetAllUsuarios();

        [OperationContract]
        Task<IEnumerable<PRO_APP_USUARIOS_CONSULTAR_PAGINADOResult>> GetUsuariosPaginado(int numeroPagina, int cantidadPorPagina);

        [OperationContract]
        Task<string> CrearUsuario(PRO_APP_USUARIOS_CONSULTAR_TODOSResult usuario);

        [OperationContract]
        Task<string> ModificarUsuario(PRO_APP_USUARIOS_CONSULTAR_TODOSResult usuario);

        [OperationContract]
        Task<string> EliminarUsuario(PRO_APP_USUARIOS_CONSULTAR_TODOSResult usuario);
    }
}
