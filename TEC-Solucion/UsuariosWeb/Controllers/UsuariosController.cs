﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using UsuariosWeb.UsuariosService;

namespace UsuariosWeb.Controllers
{
    public class UsuariosController : Controller
    {
        public async Task<ActionResult> Index()
        {
            using (UsuarioServiceClient cliente = new UsuarioServiceClient())
            {
                ViewBag.ListaUsuarios = cliente.GetAllUsuarios().ToList();
                ViewBag.DescargarListadoLink = Url.Action("DescargarListado");
                return View();
            };
        }

        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear([Bind(Include = "Nombre,Fecha_Nacimiento,Sexo")] PRO_APP_USUARIOS_CONSULTAR_TODOSResult usuario)
        {
            if (ModelState.IsValid)
            {
                using (UsuarioServiceClient cliente = new UsuarioServiceClient())
                {
                    var result = cliente.CrearUsuario(usuario).ToList();
                    return RedirectToAction("Index");
                };
            }
            return View(usuario);
        }

        public ActionResult Modificar(string nombre)
        {
            using (UsuarioServiceClient cliente = new UsuarioServiceClient())
            {
                var usuario = cliente?.GetAllUsuarios()?.FirstOrDefault(x => x.Nombre.ToUpper().Equals(nombre.ToUpper()));

                if (usuario != null)
                    return View(usuario);
                else
                    return HttpNotFound();
            };
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modificar([Bind(Include = "Nombre,Fecha_Nacimiento,Sexo")] PRO_APP_USUARIOS_CONSULTAR_TODOSResult usuario)
        {
            if (ModelState.IsValid)
            {
                using (UsuarioServiceClient cliente = new UsuarioServiceClient())
                {
                    var result = cliente.ModificarUsuario(usuario).ToList();
                    return RedirectToAction("Index");
                };
            }
            return View(usuario);
        }

        public ActionResult Eliminar(string nombre)
        {
            using (UsuarioServiceClient cliente = new UsuarioServiceClient())
            {
                var usuario = cliente?.GetAllUsuarios()?.FirstOrDefault(x => x.Nombre.ToUpper().Equals(nombre.ToUpper()));

                if (usuario != null)
                    return View(usuario);
                else
                    return HttpNotFound();
            };
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Eliminar([Bind(Include = "Nombre,Fecha_Nacimiento,Sexo")] PRO_APP_USUARIOS_CONSULTAR_TODOSResult usuario)
        {
            if (ModelState.IsValid)
            {
                using (UsuarioServiceClient cliente = new UsuarioServiceClient())
                {
                    var result = cliente.EliminarUsuario(usuario).ToList();
                    return RedirectToAction("Index");
                };
            }
            return View(usuario);
        }

        public async Task<ActionResult> DescargarListado()
        {
            IWorkbook workbook = new XSSFWorkbook();
            ISheet sheet = workbook.CreateSheet("Usuarios");

            // Encabezados de las columnas
            var headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Nombre");
            headerRow.CreateCell(1).SetCellValue("Fecha_Nacimiento");
            headerRow.CreateCell(2).SetCellValue("Sexo");
            // Agrega más encabezados de columnas según tus necesidades

            using (UsuarioServiceClient cliente = new UsuarioServiceClient())
            {
                // Datos de los técnicos
                var listadoUsuarios = cliente.GetAllUsuarios().ToList();
                for (int i = 0; i < listadoUsuarios.Count; i++)
                {
                    var usuario = listadoUsuarios[i];
                    var dataRow = sheet.CreateRow(i + 1);
                    dataRow.CreateCell(0).SetCellValue(usuario.Nombre);
                    dataRow.CreateCell(1).SetCellValue(usuario.Fecha_Nacimiento.ToString("yyyy-MM-dd"));
                    dataRow.CreateCell(2).SetCellValue(usuario.Sexo);
                    // Agrega más celdas de datos según tus necesidades
                }
            };

            // Guardar el archivo en un stream de memoria
            var stream = new MemoryStream();
            workbook.Write(stream);
            //stream.Position = 0;

            // Convertir el stream a un array de bytes
            byte[] content = stream.ToArray();

            // Devolver el archivo Excel como una descarga
            return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "usuarios.xlsx");
        }
    }
}